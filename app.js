
const todo = (value) => {
  return R.compose(
    append(text(value)),
    addClass('bg-error'),
    addClass('list-group-item'),
  )(element('div'));
};

const app = (state, container, action) => {
  container.innerHTML = '';
  const list = state.map(value => todo(value));
  list.forEach(item => container.append(item));

  const off = action((event) => {
    off();
    event.preventDefault();
    const value = elementById('text').value;
    const newState = [
      ...state,
      value,
    ];
    app(newState, container, action);
  });
};

const formSubmit = on('submit', elementById('form'));

app(
  Object.freeze([
      'Make your bed',
      'Do homework',
  ]),
  elementById('todo-list'),
  formSubmit,
);
