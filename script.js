
const form = document.getElementById('form');
const username = document.getElementById('username');
const password = document.getElementById('password');
const button = document.getElementById('button');
const alertBox = document.getElementById('alertBox');

form.addEventListener('submit', async function (event) {
  event.preventDefault();
  alertBox.hidden = true;

  const requestBody = {
    username: username.value,
    password: password.value,
  };

  const response = await fetch('https://iatc-posts-api.herokuapp.com/login', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(requestBody),
  });
  const json = await response.json();

  if (response.ok) {
    localStorage.setItem('access_token', json.accessToken);
    location.href = 'home.html';
  } else {
    alertBox.hidden = false;
    alertBox.textContent = json.message;
  }
});














// kod, yuxari qalx
