
const element = (tag) => document.createElement(tag);
const elementById = (id) => document.getElementById(id);
const text = (value) => document.createTextNode(value);

const addClass = R.curry((className, element) => {
  element.classList.add(className);
  return element;
});

const append = R.curry((child, element) => {
  element.append(child);
  return element;
});

const on = R.curry((eventType, element, callback) => {
  element.addEventListener(eventType, callback);
  return () => {
    element.removeEventListener(eventType, callback);
  };
});
